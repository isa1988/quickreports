import DrawingCanvas from './components/DrawingCanvas';

const App: React.FC = () => {
  return (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        display: 'flex',
      }}
    >
      <DrawingCanvas />
    </div>
  );
};

export default App;
