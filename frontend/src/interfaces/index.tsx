export interface IPosition {
  x: number;
  y: number;
}
export const getRussianName = (typeShape: TypeShape): string => {
  switch (typeShape) {
    case TypeShape.Circle:
      return 'Круг';
    case TypeShape.Rectangle:
      return 'Прямоугольник';
    case TypeShape.Triangle:
      return 'Треугольник';
    default:
      return '';
  }
};
export interface IShape {
  type: TypeShape;
  position: IPosition;
  fillColor: string;
  borderColor: string;
  borderWidth: number;
  index: number;
}

export interface ICircleShape extends IShape {
  type: TypeShape.Circle;
  radius: number;
}

export interface IRectangleShape extends IShape {
  type: TypeShape.Rectangle;
  width: number;
  height: number;
}

export interface ITriangleShape extends IShape {
  type: TypeShape.Triangle;
  points: IPosition[];
  sideLength: number;
}

export interface IShapeParameter {
  position: IPosition;
  fillColor: string;
  borderColor: string;
  borderWidth: number;
  index: number;
}

export interface ICircleShapeParameter extends IShapeParameter {
  radius: number;
}

export interface IRectangleShapeParameter extends IShapeParameter {
  width: number;
  height: number;
}

export interface ITriangleShapeParameter extends IShapeParameter {
  sideLength: number;
}

export interface Parameter<T> {
  value: T;
}

export enum TypeShape {
  Circle = 0,
  Rectangle = 1,
  Triangle = 2,
}
