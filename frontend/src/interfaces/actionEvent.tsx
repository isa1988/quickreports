import { IPosition, IShapeParameter, Parameter } from '.';

export interface IShapeWorkerDraw {
  draw(): JSX.Element;
}

export interface IShapeWorker {
  containsPoint(point: IPosition): boolean;
}

export interface IResize<T extends IShapeParameter> {
  resize(parameter: Parameter<T>): void;
  recolor(color: string): void;
}
