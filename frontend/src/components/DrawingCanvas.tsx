import React, { useEffect, useRef, useState } from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import {
  ICircleShape,
  IPosition,
  IRectangleShape,
  ITriangleShape,
  TypeShape,
} from '../interfaces';
import ShapeDraw from './DrawShape';
import { CircleShapeWorker } from './CircleShapeWorker';
import RectangleShapeWorker from './RectangleShapeWorker';
import TriangleEquilateralShapeWorker from './TriangleShapeWorker';
import SettingShape from './SettingShape';
import ConnectionShapeLine from './Connection';
import SettingShapeConnectionShapeLine from './SettingShapeConnectionShapeLine';

export enum TypeUpdateShapes {
  Create = 0,
  Update,
  Delete,
}

export interface DrawingCanvasType {
  shapes: ShapeDraw<any>[];
  lines: ConnectionShapeLine<any, any>[];
}

interface SelectType {
  shape: ShapeDraw<any> | null;
  line: ConnectionShapeLine<any, any> | null;
}

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <>{children}</>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const DrawingCanvas: React.FC = () => {
  const [index, setIndex] = useState(1);
  const [indexLine, setIndexLine] = useState(1);
  const [indexTabPanel, setIndexTabPanel] = useState(0);
  const [dataForm, setDataForm] = useState<DrawingCanvasType>({
    shapes: [],
    lines: [],
  });
  const [selectedShape, setSelectedShape] = useState<ShapeDraw<any> | null>(
    null
  );
  const [modifateShape, setModifateShape] = useState<SelectType>({
    shape: null,
    line: null,
  });
  const [lastMousePosition, setLastMousePosition] = useState<IPosition | null>(
    null
  );

  const canvasRef = useRef<SVGSVGElement | null>(null);
  const widthFirst = document.querySelector('.first')?.clientWidth ?? 0;

  const handleMouseDown = (e: React.MouseEvent) => {
    const point: IPosition = { x: e.clientX - widthFirst, y: e.clientY };

    for (const shape of dataForm.shapes) {
      if (shape.shapeWorker.containsPoint(point)) {
        setSelectedShape(shape);
        setModifateShape({ ...modifateShape, shape: shape });
        setLastMousePosition(point);
        break;
      }
    }
    for (const line of dataForm.lines) {
      if (line.containsPoint(point)) {
        setModifateShape({ ...modifateShape, line: line });
        break;
      }
    }
  };

  const handleMouseMove = (e: React.MouseEvent) => {
    if (selectedShape && lastMousePosition) {
      const point: IPosition = { x: e.clientX - widthFirst, y: e.clientY };
      const dx = point.x - lastMousePosition.x;
      const dy = point.y - lastMousePosition.y;

      const updatedShapes = dataForm.shapes.map((shape) => {
        if (shape === selectedShape) {
          shape.shape.position.x += dx;
          shape.shape.position.y += dy;
        }
        return shape;
      });

      setLastMousePosition(point);
      setDataForm({ ...dataForm, shapes: updatedShapes });
    }
  };

  const handleMouseUp = () => {
    setSelectedShape(null);
  };

  const modificateData = (
    shapeOrLine: ShapeDraw<any> | ConnectionShapeLine<any, any>,
    type: TypeUpdateShapes
  ) => {
    if (shapeOrLine instanceof ShapeDraw) {
      modificateShape(shapeOrLine, type);
    }
    if (shapeOrLine instanceof ConnectionShapeLine) {
      modificateLine(shapeOrLine, type);
    }
  };

  const modificateLine = (
    line: ConnectionShapeLine<any, any>,
    type: TypeUpdateShapes
  ) => {
    setDataForm((prevDataForm) => {
      switch (type) {
        case TypeUpdateShapes.Create:
          if (prevDataForm.lines.find((s) => s.index === line.index)) {
            return prevDataForm;
          } else if (
            prevDataForm.lines.find(
              (s) =>
                s.startShape === line.startShape && s.endShape === line.endShape
            )
          ) {
            return prevDataForm;
          }
          setIndexLine(line.index + 1);
          return {
            ...prevDataForm,
            lines: [...prevDataForm.lines, line],
          };
        case TypeUpdateShapes.Update:
          return {
            ...prevDataForm,
            lines: prevDataForm.lines.map((s) =>
              s.index === line.index ? line : s
            ),
          };
        case TypeUpdateShapes.Delete:
          return {
            ...prevDataForm,
            lines: prevDataForm.lines.filter((s) => s.index !== line.index),
          };
        default:
          return prevDataForm;
      }
    });
  };
  const modificateShape = (shape: ShapeDraw<any>, type: TypeUpdateShapes) => {
    setDataForm((prevDataForm) => {
      switch (type) {
        case TypeUpdateShapes.Create:
          if (
            prevDataForm.shapes.find((s) => s.shape.index === shape.shape.index)
          ) {
            return prevDataForm;
          }
          setIndex(shape.shape.index + 1);
          return {
            ...prevDataForm,
            shapes: [...prevDataForm.shapes, shape],
          };
        case TypeUpdateShapes.Update:
          return {
            ...prevDataForm,
            shapes: prevDataForm.shapes.map((s) =>
              s.shape.index === shape.shape.index ? shape : s
            ),
          };

        case TypeUpdateShapes.Delete:
          return {
            ...prevDataForm,
            shapes: prevDataForm.shapes.filter(
              (s) => s.shape.index !== shape.shape.index
            ),
            lines: prevDataForm.lines.filter(
              (s) =>
                s.startShape.shape.index !== shape.shape.index &&
                s.endShape.shape.index !== shape.shape.index
            ),
          };

        default:
          return prevDataForm;
      }
    });
  };

  useEffect(() => {
    const circleShape: ICircleShape = {
      type: TypeShape.Circle,
      position: { x: 400, y: 200 },
      radius: 50,
      fillColor: '#ff0033',
      borderColor: '#000000',
      borderWidth: 2,
      index: 1,
    };
    const circleWorker = new CircleShapeWorker(circleShape);
    const circleShapeDraw = new ShapeDraw<ICircleShape>(
      circleShape,
      circleWorker,
      circleWorker,
      circleWorker
    );

    modificateShape(circleShapeDraw, TypeUpdateShapes.Create);

    const rectangleShape: IRectangleShape = {
      type: TypeShape.Rectangle,
      position: { x: 200, y: 500 },
      width: 100,
      height: 50,
      fillColor: '#1100ff',
      borderColor: '#000000',
      borderWidth: 2,
      index: 2,
    };
    const rectangleWorker = new RectangleShapeWorker(rectangleShape);
    const rectangleShapeDraw = new ShapeDraw<IRectangleShape>(
      rectangleShape,
      rectangleWorker,
      rectangleWorker,
      rectangleWorker
    );

    modificateShape(rectangleShapeDraw, TypeUpdateShapes.Create);

    const triangleShape: ITriangleShape = {
      type: TypeShape.Triangle,
      position: { x: 400, y: 500 },
      sideLength: 100,
      points: [],
      fillColor: '#09fb5a',
      borderColor: '#000000',
      borderWidth: 2,
      index: 3,
    };
    const triangleWorker = new TriangleEquilateralShapeWorker(triangleShape);
    const triangleShapeDraw = new ShapeDraw<ITriangleShape>(
      triangleShape,
      triangleWorker,
      triangleWorker,
      triangleWorker
    );

    modificateShape(triangleShapeDraw, TypeUpdateShapes.Create);

    const connection1 = new ConnectionShapeLine(
      circleShapeDraw,
      rectangleShapeDraw,
      'black',
      2,
      1
    );

    modificateLine(connection1, TypeUpdateShapes.Create);

    const connection2 = new ConnectionShapeLine(
      rectangleShapeDraw,
      triangleShapeDraw,
      'black',
      2,
      2
    );

    modificateLine(connection2, TypeUpdateShapes.Create);

    const connection3 = new ConnectionShapeLine(
      triangleShapeDraw,
      circleShapeDraw,
      'black',
      2,
      3
    );

    modificateLine(connection3, TypeUpdateShapes.Create);
  }, []);

  const handleIndexTabPanelChange = (
    event: React.SyntheticEvent,
    newValue: number
  ) => {
    event.isTrusted;
    setIndexTabPanel(newValue);
  };

  return (
    <div className="multipleTasks">
      <div className="first">
        <Box sx={{ width: '100%' }}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs
              value={indexTabPanel}
              onChange={handleIndexTabPanelChange}
              aria-label="basic tabs example"
            >
              <Tab label="Фигуры" {...a11yProps(0)} />
              <Tab label="Линии" {...a11yProps(1)} />
            </Tabs>
          </Box>
          <CustomTabPanel value={indexTabPanel} index={0}>
            <SettingShape
              index={index}
              modificateData={modificateData}
              shapeDraw={modifateShape.shape}
            />
          </CustomTabPanel>
          <CustomTabPanel value={indexTabPanel} index={1}>
            <SettingShapeConnectionShapeLine
              shapes={dataForm.shapes}
              current={modifateShape.line}
              index={indexLine}
              modificateLine={modificateLine}
            />
          </CustomTabPanel>
        </Box>
      </div>
      <div className="second">
        <svg
          ref={canvasRef}
          width="100%"
          height="100%"
          onMouseDown={handleMouseDown}
          onMouseMove={handleMouseMove}
          onMouseUp={handleMouseUp}
          style={{ border: '1px solid black' }}
        >
          {dataForm.lines.map((shapeDraw, index) => (
            <React.Fragment key={index}> {shapeDraw.draw()}</React.Fragment>
          ))}
          {dataForm.shapes.map((shapeDraw, index) => (
            <React.Fragment key={index}>
              {shapeDraw.shapeWorkerDraw.draw()}
            </React.Fragment>
          ))}
        </svg>
      </div>
    </div>
  );
};

export default DrawingCanvas;
