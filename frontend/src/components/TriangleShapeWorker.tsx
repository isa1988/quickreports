import {
  IPosition,
  ITriangleShape,
  ITriangleShapeParameter,
  Parameter,
  TypeShape,
} from '../interfaces';
import {
  IResize,
  IShapeWorker,
  IShapeWorkerDraw,
} from '../interfaces/actionEvent';

class TriangleEquilateralShapeWorker
  implements IShapeWorker, IShapeWorkerDraw, IResize<ITriangleShapeParameter>
{
  private triangleEquilateral: ITriangleShape;

  constructor(props: ITriangleShapeParameter) {
    this.triangleEquilateral = {
      type: TypeShape.Triangle,
      position: props.position,
      fillColor: props.fillColor,
      borderColor: props.borderColor,
      borderWidth: props.borderWidth,
      sideLength: props.sideLength,
      index: props.index,
      points: [],
    };
  }

  containsPoint(point: IPosition): boolean {
    const inside = this.pointInFilledTriangle(point);

    return inside;
  }

  private pointInFilledTriangle(pt: IPosition): boolean {
    const [v1, v2, v3] = this.triangleEquilateral.points;
    const v0: IPosition = {
      x: v3.x - v1.x,
      y: v3.y - v1.y,
    };
    const v1v2: IPosition = {
      x: v2.x - v1.x,
      y: v2.y - v1.y,
    };
    const v1pt: IPosition = {
      x: pt.x - v1.x,
      y: pt.y - v1.y,
    };

    const dot00 = v0.x * v0.x + v0.y * v0.y;
    const dot01 = v0.x * v1v2.x + v0.y * v1v2.y;
    const dot02 = v0.x * v1pt.x + v0.y * v1pt.y;
    const dot11 = v1v2.x * v1v2.x + v1v2.y * v1v2.y;
    const dot12 = v1v2.x * v1pt.x + v1v2.y * v1pt.y;

    const invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
    const u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    const v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    return u >= 0 && v >= 0 && u + v <= 1 && v <= 1 - u;
  }

  draw(): JSX.Element {
    const height = (Math.sqrt(3) / 2) * this.triangleEquilateral.sideLength;

    const points: IPosition[] = [
      {
        x: this.triangleEquilateral.position.x,
        y: this.triangleEquilateral.position.y - height / 2,
      },
      {
        x:
          this.triangleEquilateral.position.x -
          this.triangleEquilateral.sideLength / 2,
        y: this.triangleEquilateral.position.y + height / 2,
      },
      {
        x:
          this.triangleEquilateral.position.x +
          this.triangleEquilateral.sideLength / 2,
        y: this.triangleEquilateral.position.y + height / 2,
      },
    ];

    const pointsAttr = points.map((p) => `${p.x},${p.y}`).join(' ');
    this.triangleEquilateral.points = points;

    const centroidX = points.reduce((acc, p) => acc + p.x, 0) / 3;
    const centroidY = points.reduce((acc, p) => acc + p.y, 0) / 3;

    return (
      <g>
        <polygon
          points={pointsAttr}
          fill={this.triangleEquilateral.fillColor}
          stroke={this.triangleEquilateral.borderColor}
          strokeWidth={this.triangleEquilateral.borderWidth}
        />
        <text
          x={centroidX}
          y={centroidY}
          fill="black"
          fontSize="12"
          textAnchor="middle"
          dominantBaseline="middle"
        >
          {this.triangleEquilateral.index.toString()}
        </text>
      </g>
    );
  }

  public resize(parameter: Parameter<ITriangleShapeParameter>): void {
    this.triangleEquilateral.sideLength = parameter.value.sideLength;
  }

  public recolor(color: string): void {
    this.triangleEquilateral.fillColor = color;
  }
}

export default TriangleEquilateralShapeWorker;
