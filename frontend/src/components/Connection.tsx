import { IPosition, IShapeParameter } from '../interfaces';
import ShapeDraw from './DrawShape';

export interface IConnectionProps<
  TStart extends IShapeParameter,
  TEnd extends IShapeParameter
> {
  startShape: ShapeDraw<TStart> | null;
  endShape: ShapeDraw<TEnd> | null;
  lineColor: string;
  lineWidth: number;
  index: number;
}

export class ConnectionShapeLine<
  TStart extends IShapeParameter,
  TEnd extends IShapeParameter
> {
  public startShape: ShapeDraw<TStart>;
  public endShape: ShapeDraw<TEnd>;
  public lineColor: string;
  public lineWidth: number;
  public index: number;

  constructor(
    startShape: ShapeDraw<TStart>,
    endShape: ShapeDraw<TEnd>,
    lineColor: string,
    lineWidth: number,
    index: number
  ) {
    this.startShape = startShape;
    this.endShape = endShape;
    this.lineColor = lineColor;
    this.lineWidth = lineWidth;
    this.index = index;
  }

  draw(): JSX.Element {
    const { startShape, endShape, lineColor, lineWidth } = this;

    return (
      <line
        x1={startShape.shape.position.x}
        y1={startShape.shape.position.y}
        x2={endShape.shape.position.x}
        y2={endShape.shape.position.y}
        stroke={lineColor}
        strokeWidth={lineWidth}
      />
    );
  }

  containsPoint(point: IPosition, tolerance: number = 5): boolean {
    const { startShape, endShape } = this;

    const x1 = startShape.shape.position.x;
    const y1 = startShape.shape.position.y;
    const x2 = endShape.shape.position.x;
    const y2 = endShape.shape.position.y;

    const dx = x2 - x1;
    const dy = y2 - y1;
    const length = Math.sqrt(dx * dx + dy * dy);

    const distance =
      Math.abs(dy * point.x - dx * point.y + x2 * y1 - y2 * x1) / length;

    return distance <= tolerance;
  }

  render() {
    return this.draw();
  }
}

export default ConnectionShapeLine;
