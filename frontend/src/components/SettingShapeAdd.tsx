import { ChangeEvent, useEffect, useState } from 'react';
import {
  getRussianName,
  ICircleShape,
  ICircleShapeParameter,
  IRectangleShape,
  IRectangleShapeParameter,
  ITriangleShape,
  ITriangleShapeParameter,
  TypeShape,
} from '../interfaces';
import ShapeDraw from './DrawShape';
import { TypeUpdateShapes } from './DrawingCanvas';
import { CircleShapeWorker } from './CircleShapeWorker';
import RectangleShapeWorker from './RectangleShapeWorker';
import TriangleEquilateralShapeWorker from './TriangleShapeWorker';

export interface SettingShapeAddProps {
  index: number;
  modificateShape: (shape: ShapeDraw<any>, type: TypeUpdateShapes) => void;
}

const SettingShapeAdd: React.FC<SettingShapeAddProps> = ({
  index,
  modificateShape,
}) => {
  const [typeShape, setTypeShape] = useState<TypeShape>(TypeShape.Circle);

  const [circleShapeParameter, setCircleShapeParameter] =
    useState<ICircleShapeParameter>({
      borderColor: '#000000',
      borderWidth: 2,
      fillColor: '#ff0033',
      index: 0,
      position: { x: 0, y: 0 },
      radius: 0,
    });

  const [rectangleShapeParameter, setRectangleShapeParameter] =
    useState<IRectangleShapeParameter>({
      borderColor: '#000000',
      borderWidth: 2,
      fillColor: '#1100ff',
      index: 0,
      position: { x: 0, y: 0 },
      width: 100,
      height: 50,
    });

  const [triangleShapeParameter, setTriangleShapeParameter] =
    useState<ITriangleShapeParameter>({
      borderColor: '#000000',
      borderWidth: 2,
      fillColor: '#09fb5a',
      index: 0,
      position: { x: 0, y: 0 },
      sideLength: 50,
    });

  useEffect(() => {
    if (typeShape.toString() === '0') {
      setTypeShape(TypeShape.Circle);
    } else if (typeShape.toString() === '1') {
      setTypeShape(TypeShape.Rectangle);
    } else if (typeShape.toString() === '2') {
      setTypeShape(TypeShape.Triangle);
    }
  }, [typeShape]);

  const handleParameterChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    switch (typeShape) {
      case TypeShape.Circle:
        setCircleShapeParameter({
          ...circleShapeParameter,
          [name]: parseFloat(value),
        });
        break;
      case TypeShape.Rectangle:
        setRectangleShapeParameter({
          ...rectangleShapeParameter,
          [name]: parseFloat(value),
        });
        break;
      case TypeShape.Triangle:
        setTriangleShapeParameter({
          ...triangleShapeParameter,
          [name]: parseFloat(value),
        });
        break;
      default:
        break;
    }
  };

  const handleColorChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    switch (typeShape) {
      case TypeShape.Circle:
        setCircleShapeParameter({
          ...circleShapeParameter,
          [name]: value,
        });
        break;
      case TypeShape.Rectangle:
        setRectangleShapeParameter({
          ...rectangleShapeParameter,
          [name]: value,
        });
        break;
      case TypeShape.Triangle:
        setTriangleShapeParameter({
          ...triangleShapeParameter,
          [name]: value,
        });
        break;
      default:
        break;
    }
  };

  const renderShapeFields = () => {
    switch (typeShape) {
      case TypeShape.Circle:
        return (
          <div>
            <label>Радиус:</label>
            <input
              type="number"
              name="radius"
              value={circleShapeParameter.radius}
              onChange={handleParameterChange}
            />
            <label>Цвет фона:</label>
            <input
              type="color"
              name="fillColor"
              value={circleShapeParameter.fillColor}
              onChange={handleColorChange}
            />
            <label>Цвет контура:</label>
            <input
              type="color"
              name="borderColor"
              value={circleShapeParameter.borderColor}
              onChange={handleColorChange}
            />
            <label>Шириев контура:</label>
            <input
              type="number"
              name="borderWidth"
              value={circleShapeParameter.borderWidth}
              onChange={handleParameterChange}
            />
            <label>Позиция:</label>
            <label>X:</label>
            <p>{circleShapeParameter.position.x}</p>
            <label>Y:</label>
            <p>{circleShapeParameter.position.y}</p>
          </div>
        );
      case TypeShape.Rectangle:
        return (
          <div>
            <label>Длина:</label>
            <input
              type="number"
              name="width"
              value={rectangleShapeParameter.width}
              onChange={handleParameterChange}
            />
            <label>Высота:</label>
            <input
              type="number"
              name="height"
              value={rectangleShapeParameter.height}
              onChange={handleParameterChange}
            />
            <label>Цвет фона:</label>
            <input
              type="color"
              name="fillColor"
              value={rectangleShapeParameter.fillColor}
              onChange={handleColorChange}
            />
            <label>Цвет контура:</label>
            <input
              type="color"
              name="borderColor"
              value={rectangleShapeParameter.borderColor}
              onChange={handleColorChange}
            />
            <label>Шириев контура:</label>
            <input
              type="number"
              name="borderWidth"
              value={rectangleShapeParameter.borderWidth}
              onChange={handleParameterChange}
            />
            <label>Позиция:</label>
            <label>X:</label>
            <p>{rectangleShapeParameter.position.x}</p>
            <label>Y:</label>
            <p>{rectangleShapeParameter.position.y}</p>
          </div>
        );
      case TypeShape.Triangle:
        return (
          <div>
            <label>Угол:</label>
            <input
              type="number"
              name="sideLength"
              value={triangleShapeParameter.sideLength}
              onChange={handleParameterChange}
            />
            <label>Цвет фона:</label>
            <input
              type="color"
              name="fillColor"
              value={triangleShapeParameter.fillColor}
              onChange={handleColorChange}
            />
            <label>Цвет контура:</label>
            <input
              type="color"
              name="borderColor"
              value={triangleShapeParameter.borderColor}
              onChange={handleColorChange}
            />
            <label>Шириев контура:</label>
            <input
              type="number"
              name="borderWidth"
              value={triangleShapeParameter.borderWidth}
              onChange={handleParameterChange}
            />
            <label>Позиция:</label>
            <label>X:</label>
            <p>{triangleShapeParameter.position.x}</p>
            <label>Y:</label>
            <p>{triangleShapeParameter.position.y}</p>
          </div>
        );
      default:
        return null;
    }
  };

  const createShape = () => {
    let newShape: ShapeDraw<any>;
    switch (typeShape) {
      case TypeShape.Circle:
        const circleShape: ICircleShape = {
          type: TypeShape.Circle,
          position: {
            x: circleShapeParameter.position.x,
            y: circleShapeParameter.position.y,
          },
          radius: circleShapeParameter.radius,
          fillColor: circleShapeParameter.fillColor,
          borderColor: circleShapeParameter.borderColor,
          borderWidth: circleShapeParameter.borderWidth,
          index: index,
        };
        const circleWorker = new CircleShapeWorker(circleShape);
        newShape = new ShapeDraw<ICircleShape>(
          circleShape,
          circleWorker,
          circleWorker,
          circleWorker
        );

        break;
      case TypeShape.Rectangle:
        const rectangleShape: IRectangleShape = {
          type: TypeShape.Rectangle,
          position: {
            x: rectangleShapeParameter.position.x,
            y: rectangleShapeParameter.position.y,
          },
          width: rectangleShapeParameter.width,
          height: rectangleShapeParameter.height,
          fillColor: rectangleShapeParameter.fillColor,
          borderColor: rectangleShapeParameter.borderColor,
          borderWidth: rectangleShapeParameter.borderWidth,
          index: index,
        };
        const rectangleWorker = new RectangleShapeWorker(rectangleShape);
        newShape = new ShapeDraw<IRectangleShape>(
          rectangleShape,
          rectangleWorker,
          rectangleWorker,
          rectangleWorker
        );
        break;
      case TypeShape.Triangle:
        const triangleShape: ITriangleShape = {
          type: TypeShape.Triangle,
          position: {
            x: triangleShapeParameter.position.x,
            y: triangleShapeParameter.position.y,
          },
          sideLength: triangleShapeParameter.sideLength,
          points: [],
          fillColor: triangleShapeParameter.fillColor,
          borderColor: triangleShapeParameter.borderColor,
          borderWidth: triangleShapeParameter.borderWidth,
          index: index,
        };
        const triangleWorker = new TriangleEquilateralShapeWorker(
          triangleShape
        );
        newShape = new ShapeDraw<ITriangleShape>(
          triangleShape,
          triangleWorker,
          triangleWorker,
          triangleWorker
        );
        break;
      default:
        return;
    }
    modificateShape(newShape, TypeUpdateShapes.Create);
  };

  const setPositionOnClick = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.isTrusted;
    const handleMouseUp = (e: MouseEvent) => {
      const { clientX: endX, clientY: endY } = e;

      const bodyRect = document.body.getBoundingClientRect();
      const offsetX =
        endX - bodyRect.left - 200 > 0 ? endX - bodyRect.left - 200 : 0;
      const offsetY = endY - bodyRect.top;

      switch (typeShape) {
        case TypeShape.Circle:
          setCircleShapeParameter({
            ...circleShapeParameter,
            position: { x: offsetX, y: offsetY },
          });
          break;
        case TypeShape.Rectangle:
          setRectangleShapeParameter({
            ...rectangleShapeParameter,
            position: { x: offsetX, y: offsetY },
          });
          break;
        case TypeShape.Triangle:
          setTriangleShapeParameter({
            ...triangleShapeParameter,
            position: { x: offsetX, y: offsetY },
          });
          break;
        default:
          break;
      }

      console.log('Clicked at:', offsetX, offsetY);

      document.removeEventListener('mouseup', handleMouseUp);
    };

    document.addEventListener('mouseup', handleMouseUp);
  };

  return (
    <div>
      <div>
        <select
          value={typeShape}
          onChange={(e) => setTypeShape(e.target.value as unknown as TypeShape)}
        >
          <option value={TypeShape.Circle}>
            {' '}
            {getRussianName(TypeShape.Circle)}{' '}
          </option>
          <option value={TypeShape.Rectangle}>
            {getRussianName(TypeShape.Rectangle)}
          </option>
          <option value={TypeShape.Triangle}>
            {getRussianName(TypeShape.Triangle)}
          </option>
        </select>
      </div>
      {renderShapeFields()}
      <button onClick={setPositionOnClick}>Установить позицию</button>
      <button onClick={createShape}>Добавить фигуру</button>
    </div>
  );
};

export default SettingShapeAdd;
