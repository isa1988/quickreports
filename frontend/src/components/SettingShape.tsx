import ShapeDraw from './DrawShape';
import { TypeUpdateShapes } from './DrawingCanvas';
import SettingShapeEdit from './SettingShapeEdit';
import SettingShapeAdd from './SettingShapeAdd';
import ConnectionShapeLine from './Connection';

interface SettingShapeProps {
  index: number;
  modificateData: (
    shape: ShapeDraw<any> | ConnectionShapeLine<any, any>,
    type: TypeUpdateShapes
  ) => void;
  shapeDraw: ShapeDraw<any> | null;
}

const SettingShape: React.FC<SettingShapeProps> = ({
  index,
  modificateData,
  shapeDraw,
}) => {
  const handleDeleteShape = () => {
    if (shapeDraw !== null) {
      modificateData(shapeDraw, TypeUpdateShapes.Delete);
      shapeDraw = null;
    }
  };
  return (
    <div>
      <SettingShapeAdd index={index} modificateShape={modificateData} />
      {shapeDraw ? (
        <div>
          <SettingShapeEdit
            shapeDraw={shapeDraw}
            modificateShape={modificateData}
          />
          <button onClick={handleDeleteShape}>Удалить фигуру</button>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default SettingShape;
