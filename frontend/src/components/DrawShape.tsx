import { IShape, IShapeParameter } from '../interfaces';
import {
  IResize,
  IShapeWorker,
  IShapeWorkerDraw,
} from '../interfaces/actionEvent';

class ShapeDraw<T extends IShapeParameter> {
  public shape: IShape;
  public resizeWorker: IResize<T>;
  public shapeWorkerDraw: IShapeWorkerDraw;
  public shapeWorker: IShapeWorker;

  constructor(
    shape: IShape,
    resizeWorker: IResize<T>,
    shapeWorkerDraw: IShapeWorkerDraw,
    shapeWorker: IShapeWorker
  ) {
    this.shape = shape;
    this.resizeWorker = resizeWorker;
    this.shapeWorkerDraw = shapeWorkerDraw;
    this.shapeWorker = shapeWorker;
  }
}

export default ShapeDraw;
