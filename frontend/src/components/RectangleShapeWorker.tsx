import {
  IPosition,
  IRectangleShape,
  IRectangleShapeParameter,
  Parameter,
  TypeShape,
} from '../interfaces';
import {
  IResize,
  IShapeWorker,
  IShapeWorkerDraw,
} from '../interfaces/actionEvent';

class RectangleShapeWorker
  implements IShapeWorker, IShapeWorkerDraw, IResize<IRectangleShapeParameter>
{
  private rectangleShape: IRectangleShape;

  constructor(props: IRectangleShapeParameter) {
    this.rectangleShape = {
      type: TypeShape.Rectangle,
      position: props.position,
      fillColor: props.fillColor,
      borderColor: props.borderColor,
      borderWidth: props.borderWidth,
      width: props.width,
      height: props.height,
      index: props.index,
    };
  }

  containsPoint(point: IPosition): boolean {
    return (
      point.x >= this.rectangleShape.position.x &&
      point.x <= this.rectangleShape.position.x + this.rectangleShape.width &&
      point.y >= this.rectangleShape.position.y &&
      point.y <= this.rectangleShape.position.y + this.rectangleShape.height
    );
  }

  draw(): JSX.Element {
    return (
      <g>
        <rect
          x={this.rectangleShape.position.x}
          y={this.rectangleShape.position.y}
          width={this.rectangleShape.width}
          height={this.rectangleShape.height}
          fill={this.rectangleShape.fillColor}
          stroke={this.rectangleShape.borderColor}
          strokeWidth={this.rectangleShape.borderWidth}
        />
        <text
          x={this.rectangleShape.position.x + this.rectangleShape.width / 2}
          y={this.rectangleShape.position.y + this.rectangleShape.height / 2}
          fill="black"
          fontSize="12"
          textAnchor="middle"
          dominantBaseline="middle"
        >
          {this.rectangleShape.index}
        </text>
      </g>
    );
  }

  resize(parameter: Parameter<IRectangleShapeParameter>): void {
    this.rectangleShape.width = parameter.value.width;
    this.rectangleShape.height = parameter.value.height;
  }

  public recolor(color: string): void {
    this.rectangleShape.fillColor = color;
  }
}

export default RectangleShapeWorker;
