import { ChangeEvent, useEffect, useState } from 'react';
import ConnectionShapeLine, { IConnectionProps } from './Connection';
import { TypeUpdateShapes } from './DrawingCanvas';
import ShapeDraw from './DrawShape';
import { getRussianName, TypeShape } from '../interfaces';

interface ISettingShapeConnectionShapeLineProps {
  shapes: ShapeDraw<any>[];
  current: ConnectionShapeLine<any, any> | null;
  index: number;
  modificateLine: (
    connection: ConnectionShapeLine<any, any>,
    type: TypeUpdateShapes
  ) => void;
}

enum TypeField {
  add = 0,
  edit,
}

const SettingShapeConnectionShapeLine: React.FC<
  ISettingShapeConnectionShapeLineProps
> = ({ shapes, current, index, modificateLine: modificateShape }) => {
  const [addItem, setAddItem] = useState<IConnectionProps<any, any>>({
    startShape: null,
    endShape: null,
    lineColor: 'black',
    lineWidth: 2,
    index: 0,
  });

  useEffect(() => {}, [shapes, current]);

  const handleAddConnection = () => {
    if (addItem.startShape !== null && addItem.endShape !== null) {
      const connection = new ConnectionShapeLine(
        addItem.startShape,
        addItem.endShape,
        addItem.lineColor,
        addItem.lineWidth,
        index
      );
      modificateShape(connection, TypeUpdateShapes.Create);
    }
  };

  const handleColorChange = (
    event: ChangeEvent<HTMLInputElement>,
    typeShape: TypeField
  ) => {
    const { name, value } = event.target;
    switch (typeShape) {
      case TypeField.add:
        setAddItem({
          ...addItem,
          [name]: value,
        });
        break;
      case TypeField.edit:
        if (current !== null) {
          current.lineColor = value;
        }
        break;
      default:
        break;
    }
  };

  const handleDelete = () => {
    if (
      current !== null &&
      current.startShape !== null &&
      current.endShape !== null
    ) {
      modificateShape(current, TypeUpdateShapes.Delete);
      current = null;
    }
  };

  const handleShapeChange = (
    e: React.ChangeEvent<HTMLSelectElement>,
    type: 'startShape' | 'endShape'
  ) => {
    const shapeIndex = parseInt(e.target.value);
    const selectedShape =
      shapes.find((shape) => shape.shape.index === shapeIndex) || null;
    setAddItem((prevState) => ({
      ...prevState,
      [type]: selectedShape,
    }));
  };

  const handleRecolorConnection = () => {
    if (
      current !== null &&
      current.startShape !== null &&
      current.endShape !== null
    ) {
      modificateShape(current, TypeUpdateShapes.Update);
    }
  };

  return (
    <div>
      <div>
        <label>Начальная точка:</label>
        <select
          value={addItem.startShape?.shape.index || ''}
          onChange={(e) => handleShapeChange(e, 'startShape')}
        >
          <option value="">Выберите фигуру</option>
          {shapes.map((shape) => (
            <option key={shape.shape.index} value={shape.shape.index}>
              {getRussianName(shape.shape.type)} - {shape.shape.index}
            </option>
          ))}
        </select>
      </div>
      <div>
        <label>Конечная точка:</label>
        <select
          value={addItem.endShape?.shape.index || ''}
          onChange={(e) => handleShapeChange(e, 'endShape')}
        >
          <option value="">Выберите фигуру</option>
          {shapes.map((shape) => (
            <option key={shape.shape.index} value={shape.shape.index}>
              {getRussianName(shape.shape.type)} - {shape.shape.index}
            </option>
          ))}
        </select>
      </div>
      <div>
        <label>Цвет</label>
        <input
          type="color"
          name="lineColor"
          value={addItem.lineColor}
          onChange={(e) => handleColorChange(e, TypeField.add)}
        />
      </div>
      <div>
        <label>Толщина линии</label>
        <input
          type="number"
          name="lineWidth"
          value={addItem.lineWidth}
          onChange={(e) => handleColorChange(e, TypeField.add)}
        />
      </div>
      <button onClick={handleAddConnection}>Добавить линию</button>
      {current && (
        <div>
          <h3>Редактировать</h3>
          <p>
            Начальная точка:{' '}
            {getRussianName(
              current.startShape?.shape?.type ?? TypeShape.Circle
            )}{' '}
            - {current.startShape?.shape?.index}
          </p>
          <p>
            Конечная точка:{' '}
            {getRussianName(current.endShape?.shape?.type ?? TypeShape.Circle)}{' '}
            - {current.endShape?.shape?.index}
          </p>

          <div>
            <label>Цвет</label>
            <input
              type="color"
              name="lineColor"
              value={current.lineColor}
              onChange={(e) => handleColorChange(e, TypeField.edit)}
            />
          </div>

          <button onClick={handleRecolorConnection}>Изменить цвет</button>
          <button onClick={handleDelete}>Удалить линию</button>
        </div>
      )}
    </div>
  );
};

export default SettingShapeConnectionShapeLine;
