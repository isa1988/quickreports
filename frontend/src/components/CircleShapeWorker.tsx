import {
  ICircleShape,
  ICircleShapeParameter,
  Parameter,
  TypeShape,
} from '../interfaces';
import {
  IResize,
  IShapeWorker,
  IShapeWorkerDraw,
} from '../interfaces/actionEvent';

export class CircleShapeWorker
  implements IShapeWorker, IShapeWorkerDraw, IResize<ICircleShapeParameter>
{
  private circleShape: ICircleShape;

  constructor(props: ICircleShapeParameter) {
    this.circleShape = {
      type: TypeShape.Circle,
      position: props.position,
      fillColor: props.fillColor,
      borderColor: props.borderColor,
      borderWidth: props.borderWidth,
      radius: props.radius,
      index: props.index,
    };
  }

  public containsPoint(point: { x: number; y: number }): boolean {
    const dx = point.x - this.circleShape.position.x;
    const dy = point.y - this.circleShape.position.y;
    return Math.sqrt(dx * dx + dy * dy) <= this.circleShape.radius;
  }

  public draw(): JSX.Element {
    return (
      <g>
        <circle
          cx={this.circleShape.position.x}
          cy={this.circleShape.position.y}
          r={this.circleShape.radius}
          fill={this.circleShape.fillColor}
          stroke={this.circleShape.borderColor}
          strokeWidth={this.circleShape.borderWidth}
        />
        <text
          x={this.circleShape.position.x}
          y={this.circleShape.position.y}
          fill="black"
          fontSize="12"
          textAnchor="middle"
          dominantBaseline="middle"
        >
          {this.circleShape.index}
        </text>
      </g>
    );
  }

  public resize(parameter: Parameter<ICircleShapeParameter>): void {
    if (parameter.value.radius <= 0) {
      parameter.value.radius = 1;
    }
    this.circleShape.radius = parameter.value.radius;
  }

  public recolor(color: string): void {
    this.circleShape.fillColor = color;
  }
}
