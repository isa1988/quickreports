import { ChangeEvent, useEffect, useState } from 'react';
import { TypeUpdateShapes } from './DrawingCanvas';
import ShapeDraw from './DrawShape';
import {
  ICircleShape,
  ICircleShapeParameter,
  IRectangleShape,
  IRectangleShapeParameter,
  ITriangleShape,
  ITriangleShapeParameter,
  Parameter,
  TypeShape,
} from '../interfaces';

export interface SettingShapeEditProps {
  shapeDraw: ShapeDraw<any>;
  modificateShape: (shape: ShapeDraw<any>, type: TypeUpdateShapes) => void;
}

const SettingShapeEdit: React.FC<SettingShapeEditProps> = ({
  shapeDraw,
  modificateShape,
}) => {
  const [circleShapeParameter, setCircleShapeParameter] =
    useState<ICircleShapeParameter>({
      borderColor: shapeDraw.shape.borderColor,
      borderWidth: shapeDraw.shape.borderWidth,
      fillColor: shapeDraw.shape.fillColor,
      index: shapeDraw.shape.index,
      position: { ...shapeDraw.shape.position },
      radius:
        'radius' in shapeDraw.shape
          ? (shapeDraw.shape as ICircleShape).radius
          : 0,
    });

  const [rectangleShapeParameter, setRectangleShapeParameter] =
    useState<IRectangleShapeParameter>({
      borderColor: 'black',
      borderWidth: 2,
      fillColor: 'blue',
      index: 0,
      position: { x: 0, y: 0 },
      width: 100,
      height: 50,
    });

  const [triangleShapeParameter, setTriangleShapeParameter] =
    useState<ITriangleShapeParameter>({
      borderColor: 'black',
      borderWidth: 2,
      fillColor: 'green',
      index: 0,
      position: { x: 0, y: 0 },
      sideLength: 50,
    });

  useEffect(() => {
    switch (shapeDraw.shape.type) {
      case TypeShape.Circle:
        setCircleShapeParameter({
          borderColor: shapeDraw.shape.borderColor,
          borderWidth: shapeDraw.shape.borderWidth,
          fillColor: shapeDraw.shape.fillColor,
          index: shapeDraw.shape.index,
          position: { ...shapeDraw.shape.position },
          radius:
            'radius' in shapeDraw.shape
              ? (shapeDraw.shape as ICircleShape).radius
              : 0,
        });
        break;
      case TypeShape.Rectangle:
        setRectangleShapeParameter({
          borderColor: shapeDraw.shape.borderColor,
          borderWidth: shapeDraw.shape.borderWidth,
          fillColor: shapeDraw.shape.fillColor,
          index: shapeDraw.shape.index,
          position: { ...shapeDraw.shape.position },
          width:
            'width' in shapeDraw.shape
              ? (shapeDraw.shape as IRectangleShape).width
              : 0,
          height:
            'height' in shapeDraw.shape
              ? (shapeDraw.shape as IRectangleShape).height
              : 0,
        });
        break;
      case TypeShape.Triangle:
        setTriangleShapeParameter({
          borderColor: shapeDraw.shape.borderColor,
          borderWidth: shapeDraw.shape.borderWidth,
          fillColor: shapeDraw.shape.fillColor,
          index: shapeDraw.shape.index,
          position: { ...shapeDraw.shape.position },
          sideLength:
            'sideLength' in shapeDraw.shape
              ? (shapeDraw.shape as ITriangleShape).sideLength
              : 0,
        });
        break;
    }
  }, [shapeDraw.shape]);

  const handleParameterChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    switch (shapeDraw.shape.type) {
      case TypeShape.Circle:
        setCircleShapeParameter({
          ...circleShapeParameter,
          [name]: parseFloat(value),
        });
        break;
      case TypeShape.Rectangle:
        setRectangleShapeParameter({
          ...rectangleShapeParameter,
          [name]: parseFloat(value),
        });
        break;
      case TypeShape.Triangle:
        setTriangleShapeParameter({
          ...triangleShapeParameter,
          [name]: parseFloat(value),
        });
        break;
      default:
        break;
    }
  };

  const handleColorChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    switch (shapeDraw.shape.type) {
      case TypeShape.Circle:
        setCircleShapeParameter({
          ...circleShapeParameter,
          [name]: value,
        });
        break;
      case TypeShape.Rectangle:
        setRectangleShapeParameter({
          ...rectangleShapeParameter,
          [name]: value,
        });
        break;
      case TypeShape.Triangle:
        setTriangleShapeParameter({
          ...triangleShapeParameter,
          [name]: value,
        });
        break;
      default:
        break;
    }
  };

  const renderShapeFields = () => {
    switch (shapeDraw.shape.type) {
      case TypeShape.Circle:
        return (
          <div>
            <label>Радиус:</label>
            <input
              type="number"
              name="radius"
              value={circleShapeParameter.radius}
              onChange={handleParameterChange}
            />
            <label>Цвет фона:</label>
            <input
              type="color"
              name="fillColor"
              value={circleShapeParameter.fillColor}
              onChange={handleColorChange}
            />
          </div>
        );
      case TypeShape.Rectangle:
        return (
          <div>
            <label>Длина:</label>
            <input
              type="number"
              name="width"
              value={rectangleShapeParameter.width}
              onChange={handleParameterChange}
            />
            <label>Высота:</label>
            <input
              type="number"
              name="height"
              value={rectangleShapeParameter.height}
              onChange={handleParameterChange}
            />
            <label>Цвет фона:</label>
            <input
              type="color"
              name="fillColor"
              value={rectangleShapeParameter.fillColor}
              onChange={handleColorChange}
            />
          </div>
        );
      case TypeShape.Triangle:
        return (
          <div>
            <label>Угол:</label>
            <input
              type="number"
              name="sideLength"
              value={triangleShapeParameter.sideLength}
              onChange={handleParameterChange}
            />
            <label>Цвет фона:</label>
            <input
              type="color"
              name="fillColor"
              value={triangleShapeParameter.fillColor}
              onChange={handleColorChange}
            />
          </div>
        );
      default:
        return null;
    }
  };

  const resize = () => {
    let parameters: Parameter<any>;
    switch (shapeDraw.shape.type) {
      case TypeShape.Circle:
        parameters = {
          value: circleShapeParameter,
        };

        break;
      case TypeShape.Rectangle:
        parameters = {
          value: rectangleShapeParameter,
        };
        break;
      case TypeShape.Triangle:
        parameters = {
          value: triangleShapeParameter,
        };
        break;
      default:
        return;
    }
    shapeDraw.resizeWorker.resize(parameters);
    modificateShape(shapeDraw, TypeUpdateShapes.Update);
  };

  const recolor = () => {
    let parameters: string;
    switch (shapeDraw.shape.type) {
      case TypeShape.Circle:
        parameters = circleShapeParameter.fillColor;

        break;
      case TypeShape.Rectangle:
        parameters = rectangleShapeParameter.fillColor;
        break;
      case TypeShape.Triangle:
        parameters = triangleShapeParameter.fillColor;
        break;
      default:
        return;
    }
    shapeDraw.resizeWorker.recolor(parameters);
    modificateShape(shapeDraw, TypeUpdateShapes.Update);
  };

  return (
    <div>
      {renderShapeFields()}
      <button onClick={resize}>Изменить размер</button>
      <button onClick={recolor}>Изменить цвет</button>
    </div>
  );
};

export default SettingShapeEdit;
