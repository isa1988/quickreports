using OperationsWithShapes.Circle;
using OperationsWithShapes.Triangle;
using OperationsWithShapes;
using WinFormsApp.WorkWithShape;
using WinFormsApp.WorkWithShape.Circle;
using WinFormsApp.WorkWithShape.Square;
using WinFormsApp.WorkWithShape.Triangle;
using OperationsWithShapes.BaseLogic;
using OperationsWithShapes.Square;

namespace WinFormsApp
{
    public partial class Form1 : Form
    {
        private ShapeWorker shapeWorker;
        public Form1()
        {

            InitializeComponent();

            
            shapeWorker = new ShapeWorker();
            shapeWorker.Dock = DockStyle.Fill; 
            Controls.Add(shapeWorker); 

            
            AddSampleShapes();
        }

        private void AddSampleShapes()
        {
            CircleShape circleShape = new CircleShape(new CircleShapeParameter() { Position = new PointF(100, 100), Radius = 50, FillColor = Color.Blue, BorderColor = Color.Black, BorderWidth = 2 });
            CircleShapeWorker circleShapeWorker = new CircleShapeWorker(circleShape);
            ShapeDraw<CircleShapeParameter> circleShapeDraw = new ShapeDraw<CircleShapeParameter>(circleShape, circleShapeWorker, circleShapeWorker, circleShapeWorker);

            RectangleShape rectangleShape = new RectangleShape(new RectangleShapeParameter { 
                Position = new PointF(200, 200), Width = 100, Height = 80, FillColor = Color.Green, BorderColor = Color.Black, BorderWidth = 2 });
            RectangleShapeWorker rectangleShapeWorker = new RectangleShapeWorker(rectangleShape);
            ShapeDraw<RectangleShapeParameter> rectangleShapeDraw = new ShapeDraw<RectangleShapeParameter>(rectangleShape, rectangleShapeWorker, rectangleShapeWorker, rectangleShapeWorker);

            TriangleEquilateralShape triangleShape = new TriangleEquilateralShape(new TriangleEquilateralShapeParameter {
                Position = new PointF(300, 300), SideLength = 80, FillColor = Color.Red, BorderColor = Color.Black, BorderWidth = 2 });
            TriangleEquilateralShapeWorker triangleShapeWorker = new TriangleEquilateralShapeWorker(triangleShape);
            ShapeDraw<TriangleEquilateralShapeParameter> triangleShapeDraw = new ShapeDraw<TriangleEquilateralShapeParameter>(triangleShape, triangleShapeWorker, triangleShapeWorker, triangleShapeWorker);

            
            shapeWorker.AddShape(circleShapeDraw);
            shapeWorker.AddShape(rectangleShapeDraw);
            shapeWorker.AddShape(triangleShapeDraw);


            shapeWorker.AddConnection(new Connection<CircleShapeParameter, RectangleShapeParameter>(
                circleShapeDraw, rectangleShapeDraw, Color.Black, 2));

            shapeWorker.AddConnection(new Connection<RectangleShapeParameter, TriangleEquilateralShapeParameter>(
                rectangleShapeDraw, triangleShapeDraw, Color.Blue, 2));

            shapeWorker.AddConnection(new Connection<TriangleEquilateralShapeParameter, CircleShapeParameter>(
                triangleShapeDraw, circleShapeDraw, Color.Green, 2));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
