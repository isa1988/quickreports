﻿using OperationsWithShapes.BaseLogic;

namespace WinFormsApp.WorkWithShape;

public class ShapeDraw<T>
    where T : class
{
    public Shape Shape { get; private set; }
    public IResize<T> ResizeWorker { get; private set; }
    public IShapeWorkerDraw ShapeWorkerDraw { get; private set; }
    public IShapeWorker ShapeWorker { get; private set; }
    public ShapeDraw(Shape shape, IResize<T> resizeWorker, IShapeWorkerDraw shapeWorkerDraw, IShapeWorker shapeWorker)
    {
        this.Shape = shape;
        this.ResizeWorker = resizeWorker;
        this.ShapeWorkerDraw = shapeWorkerDraw;
        this.ShapeWorker = shapeWorker;
    }
}
