﻿namespace WinFormsApp.WorkWithShape;

public class Connection<TStart, TEnd>
where TStart : class
where TEnd : class
{
    public ShapeDraw<TStart> StartShape { get; set; }
    public ShapeDraw<TEnd> EndShape { get; set; }
    public Color LineColor { get; set; }
    public int LineWidth { get; set; }

    public Connection(ShapeDraw<TStart> startShape, ShapeDraw<TEnd> endShape, Color lineColor, int lineWidth)
    {
        StartShape = startShape;
        EndShape = endShape;
        LineColor = lineColor;
        LineWidth = lineWidth;
    }

    public void Draw(Graphics graphics)
    {
        using (Pen pen = new Pen(LineColor, LineWidth))
        {
            graphics.DrawLine(pen, StartShape.Shape.Position, EndShape.Shape.Position);
        }
    }
}


