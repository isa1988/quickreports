﻿using OperationsWithShapes.BaseLogic;
using OperationsWithShapes.Circle;

namespace WinFormsApp.WorkWithShape.Circle;

public class CircleShapeWorker : IResize<CircleShapeParameter>, IShapeWorker, IShapeWorkerDraw
{
    private readonly CircleShape circleShape;

    public CircleShapeWorker(CircleShape circleShape)
    {
        this.circleShape = circleShape;
    }

    public bool ContainsPoint(PointF point)
    {
        float dx = point.X - (circleShape.Position.X + circleShape.Radius);
        float dy = point.Y - (circleShape.Position.Y + circleShape.Radius);
        return dx * dx + dy * dy <= circleShape.Radius * circleShape.Radius;
    }

    public Image Draw()
    {
        int diameter = (int)(2 * circleShape.Radius);
        Bitmap bitmap = new Bitmap(diameter, diameter);

        using (Graphics graphics = Graphics.FromImage(bitmap))
        {
            graphics.FillEllipse(new SolidBrush(circleShape.FillColor), 0, 0, diameter - 1, diameter - 1);

            using (Pen pen = new Pen(circleShape.BorderColor, circleShape.BorderWidth))
            {
                graphics.DrawEllipse(pen, 0, 0, diameter - 1, diameter - 1);
            }
        }

        return bitmap;
    }

    public void Resize(Parameter<CircleShapeParameter> parameter)
    {
        if (parameter.Value.Radius <= 0)
        {
            parameter.Value.Radius = 1;
        }
        circleShape.SetRadios(parameter.Value.Radius);
    }
}
