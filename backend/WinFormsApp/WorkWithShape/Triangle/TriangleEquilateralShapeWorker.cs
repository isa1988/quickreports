﻿using OperationsWithShapes.BaseLogic;
using OperationsWithShapes.Triangle;
namespace WinFormsApp.WorkWithShape.Triangle;

public class TriangleEquilateralShapeWorker : IResize<TriangleEquilateralShapeParameter>, IShapeWorker, IShapeWorkerDraw
{
    private readonly TriangleEquilateralShape triangleEquilateral;

    public TriangleEquilateralShapeWorker(TriangleEquilateralShape triangleEquilateral)
    {
        this.triangleEquilateral = triangleEquilateral;
    }

    public bool ContainsPoint(PointF point)
    {
        PointF[] vertices = CalculateVertices();

        bool inside = PointInFilledTriangle(point, vertices[0], vertices[1], vertices[2]);

        return inside;
    }

    private bool PointInFilledTriangle(PointF pt, PointF v1, PointF v2, PointF v3)
    {
        PointF v0 = new PointF(v3.X - v1.X, v3.Y - v1.Y);
        PointF v1v2 = new PointF(v2.X - v1.X, v2.Y - v1.Y);
        PointF v1pt = new PointF(pt.X - v1.X, pt.Y - v1.Y);

        float dot00 = v0.X * v0.X + v0.Y * v0.Y;
        float dot01 = v0.X * v1v2.X + v0.Y * v1v2.Y;
        float dot02 = v0.X * v1pt.X + v0.Y * v1pt.Y;
        float dot11 = v1v2.X * v1v2.X + v1v2.Y * v1v2.Y;
        float dot12 = v1v2.X * v1pt.X + v1v2.Y * v1pt.Y;

        float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
        float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
        float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

        return (u >= 0) && (v >= 0) && (u + v <= 1) && (v <= 1 - u); 
    }

    private PointF[] CalculateVertices()
    {
        PointF A = new PointF(triangleEquilateral.Position.X + triangleEquilateral.Points[0].X, triangleEquilateral.Position.Y + +triangleEquilateral.Points[0].Y); 
        PointF B = new PointF(triangleEquilateral.Position.X + +triangleEquilateral.Points[1].X, triangleEquilateral.Position.Y + triangleEquilateral.Points[1].Y); 
        PointF C = new PointF(triangleEquilateral.Position.X + +triangleEquilateral.Points[2].X, triangleEquilateral.Position.Y + +triangleEquilateral.Points[2].Y); 

        return new PointF[] { A, B, C };
    }


    public Image Draw()
    {
        float height = (float)(Math.Sqrt(3) / 2 * triangleEquilateral.SideLength);
        Bitmap bitmap = new Bitmap((int)triangleEquilateral.SideLength, (int)height);
        using (Graphics graphics = Graphics.FromImage(bitmap))
        {
            PointF[] points = new PointF[3];
            points[0] = new PointF(triangleEquilateral.SideLength / 2, 0);
            points[1] = new PointF(0, height);
            points[2] = new PointF(triangleEquilateral.SideLength, height);

            using (SolidBrush brush = new SolidBrush(triangleEquilateral.FillColor))
            {
                graphics.FillPolygon(brush, points);
            }
            using (Pen pen = new Pen(triangleEquilateral.BorderColor, triangleEquilateral.BorderWidth))
            {
                graphics.DrawPolygon(pen, points);
            }

            triangleEquilateral.SetPoints(points);
        }
        return bitmap;
    }

    public void Resize(Parameter<TriangleEquilateralShapeParameter> parameter)

    {
        if (parameter.Value.SideLength <= 1)
        {
            parameter.Value.SideLength = 2;
        }

        triangleEquilateral.SetSideLength(parameter.Value.SideLength);
    }
}

