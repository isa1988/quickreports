﻿
using OperationsWithShapes.BaseLogic;

namespace WinFormsApp.WorkWithShape;

public interface IResize<T> 
    where T: class
{
    public abstract void Resize(Parameter<T> parameter);
}
