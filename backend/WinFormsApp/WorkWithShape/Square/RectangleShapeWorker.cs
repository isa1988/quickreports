﻿using OperationsWithShapes;
using OperationsWithShapes.BaseLogic;
using OperationsWithShapes.Square;
using System.Drawing.Drawing2D;

namespace WinFormsApp.WorkWithShape.Square
{
    public class RectangleShapeWorker : IResize<RectangleShapeParameter>, IShapeWorker, IShapeWorkerDraw
    {
        private readonly RectangleShape rectangleShape;

        public RectangleShapeWorker(RectangleShape rectangleShape)
        {
            this.rectangleShape = rectangleShape;
        }
        public bool ContainsPoint(PointF point)
        {
            return point.X >= rectangleShape.Position.X && point.X <= rectangleShape.Position.X + rectangleShape.Width &&
               point.Y >= rectangleShape.Position.Y && point.Y <= rectangleShape.Position.Y + rectangleShape.Height;
        }

        public Image Draw()
        {
            Bitmap bitmap = new Bitmap((int)rectangleShape.Width, (int)rectangleShape.Height);

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;

                using (SolidBrush brush = new SolidBrush(rectangleShape.FillColor))
                {
                    graphics.FillRectangle(brush, 0, 0, (int)rectangleShape.Width, (int)rectangleShape.Height);
                }

                using (Pen pen = new Pen(rectangleShape.BorderColor, rectangleShape.BorderWidth))
                {
                    graphics.DrawLine(pen, 0, 0, rectangleShape.Width, 0);
                    graphics.DrawLine(pen, 0, rectangleShape.Height - 1, rectangleShape.Width, rectangleShape.Height - 1);
                    graphics.DrawLine(pen, 0, 0, 0, rectangleShape.Height);
                    graphics.DrawLine(pen, rectangleShape.Width - 1, 0, rectangleShape.Width - 1, rectangleShape.Height);
                }
            }

            return bitmap;
        }

        public void Resize(Parameter<RectangleShapeParameter> rectangle)
        {
            if (rectangle.Value.Width <= 0)
            {
                rectangle.Value.Width = 1;
            }
            if (rectangle.Value.Height <= 0) 
            {
                rectangle.Value.Height = 1; 
            }
            rectangleShape.SetWidth(rectangle.Value.Width);
            rectangleShape.SetHeight(rectangle.Value.Height);
        }
    }
}
