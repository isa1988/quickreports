﻿namespace WinFormsApp.WorkWithShape;

public interface IShapeWorkerDraw
{
    Image Draw();
}
