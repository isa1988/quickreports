﻿namespace WinFormsApp.WorkWithShape;

public interface IShapeWorker
{
    bool ContainsPoint(PointF point);
}
