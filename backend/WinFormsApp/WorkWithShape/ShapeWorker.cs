﻿using OperationsWithShapes;
using OperationsWithShapes.BaseLogic;
using OperationsWithShapes.Circle;
using OperationsWithShapes.Square;
using OperationsWithShapes.Triangle;

namespace WinFormsApp.WorkWithShape;

public class ShapeWorker : Panel
{
    private List<object> shapes = new List<object>();
    private List<object> connections = new List<object>();
    private object selectedShape = null;
    private PointF lastMousePosition;
    private bool isResizing = false;
    private const int ResizeThreshold = 10;

    public ShapeWorker()
    {
        this.DoubleBuffered = true;
        this.ResizeRedraw = true;

        this.MouseDown += ShapeWorker_MouseDown;
        this.MouseMove += ShapeWorker_MouseMove;
        this.MouseUp += ShapeWorker_MouseUp;
    }

    public void AddShape<T>(ShapeDraw<T> shape) where T : class
    {
        shapes.Add(shape);
        Invalidate();
    }

    public void RemoveShape<T>(ShapeDraw<T> shape) where T : class
    {
        shapes.Remove(shape);
        Invalidate();
    }

    public void AddConnection<TStart, TEnd>(Connection<TStart, TEnd> connection)
        where TStart : class
        where TEnd : class
    {
        connections.Add(connection);
        Invalidate();
    }

    protected override void OnPaint(PaintEventArgs e)
    {
        base.OnPaint(e);

        foreach (var connection in connections)
        {
            dynamic dynamicConnection = connection;
            dynamicConnection.Draw(e.Graphics);
        }

        foreach (var obj in shapes)
        {
            dynamic shapeDraw = obj;
            var img = shapeDraw.ShapeWorkerDraw.Draw();
            e.Graphics.DrawImage(img, shapeDraw.Shape.Position);
        }
    }

    private void ShapeWorker_MouseDown(object sender, MouseEventArgs e)
    {
        foreach (var obj in shapes)
        {
            dynamic shapeDraw = obj;
            if (shapeDraw.ShapeWorker.ContainsPoint(e.Location))
            {
                selectedShape = shapeDraw;
                lastMousePosition = e.Location;

                if (IsNearBorder(shapeDraw.Shape, e.Location))
                {
                    isResizing = true;
                }
                break;
            }
        }
    }

    private void ShapeWorker_MouseMove(object sender, MouseEventArgs e)
    {
        if (selectedShape != null && e.Button == MouseButtons.Left)
        {
            if (isResizing)
            {
                ResizeSelectedShape(e.Location);
            }
            else
            {
                MoveSelectedShape(e.Location);
            }
            lastMousePosition = e.Location;
            Invalidate();
        }
    }

    private void ShapeWorker_MouseUp(object sender, MouseEventArgs e)
    {
        selectedShape = null;
        isResizing = false;
    }

    private bool IsNearBorder(Shape shape, PointF point)
    {
        if (shape is CircleShape circle)
        {
            float dx = point.X - circle.Position.X;
            float dy = point.Y - circle.Position.Y;
            float distance = (float)Math.Sqrt(dx * dx + dy * dy);
            return Math.Abs(distance - circle.Radius) <= ResizeThreshold;
        }
        else if (shape is RectangleShape rectangle)
        {
            return (Math.Abs(point.X - rectangle.Position.X) <= ResizeThreshold ||
                    Math.Abs(point.X - (rectangle.Position.X + rectangle.Width)) <= ResizeThreshold ||
                    Math.Abs(point.Y - rectangle.Position.Y) <= ResizeThreshold ||
                    Math.Abs(point.Y - (rectangle.Position.Y + rectangle.Height)) <= ResizeThreshold);
        }
        else if (shape is TriangleEquilateralShape triangle)
        {
            return (Math.Abs(point.X - triangle.Position.X) <= ResizeThreshold ||
                    Math.Abs(point.X - (triangle.Position.X + triangle.SideLength)) <= ResizeThreshold ||
                    Math.Abs(point.Y - triangle.Position.Y) <= ResizeThreshold ||
                    Math.Abs(point.Y - (triangle.Position.Y + (float)(Math.Sqrt(3) / 2 * triangle.SideLength))) <= ResizeThreshold);
        }
        return false;
    }

    private void ResizeSelectedShape(PointF point)
    {
        if (selectedShape is ShapeDraw<CircleShapeParameter> circleShapeDraw)
        {
            float newRadius = Math.Abs(point.X - circleShapeDraw.Shape.Position.X);
            var parameter = new Parameter<CircleShapeParameter>() { Value = new CircleShapeParameter { Radius = newRadius } };
            ResizeShape(circleShapeDraw, parameter);
        }
        else if (selectedShape is ShapeDraw<RectangleShapeParameter> rectangleShapeDraw)
        {
            float newWidth = Math.Abs(point.X - rectangleShapeDraw.Shape.Position.X);
            float newHeight = Math.Abs(point.Y - rectangleShapeDraw.Shape.Position.Y);
            var parameter = new Parameter<RectangleShapeParameter> { Value = new RectangleShapeParameter { Width = newWidth, Height = newHeight } };
            ResizeShape(rectangleShapeDraw, parameter);
        }
        else if (selectedShape is ShapeDraw<TriangleEquilateralShapeParameter> triangleShapeDraw)
        {
            float newSideLength = Math.Abs(point.X - triangleShapeDraw.Shape.Position.X);
            var parameter = new Parameter<TriangleEquilateralShapeParameter> { Value = new TriangleEquilateralShapeParameter { SideLength = newSideLength } };
            ResizeShape(triangleShapeDraw, parameter);
        }
    }

    private void MoveSelectedShape(PointF point)
    {
        dynamic shapeDraw = selectedShape;
        var dx = point.X - lastMousePosition.X;
        var dy = point.Y - lastMousePosition.Y;
        var newPos = new PointF(shapeDraw.Shape.Position.X + dx, shapeDraw.Shape.Position.Y + dy);
        shapeDraw.Shape.Position = newPos;
    }

    public void ResizeShape<T>(ShapeDraw<T> shapeDraw, Parameter<T> parameter) where T : class
    {
        shapeDraw.ResizeWorker.Resize(parameter);
        Invalidate();
    }
}