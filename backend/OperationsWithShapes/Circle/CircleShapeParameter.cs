﻿
using OperationsWithShapes.BaseLogic;

namespace OperationsWithShapes.Circle;

public record CircleShapeParameter : ShapeParameter
{
    public float Radius { get; set; }
}
