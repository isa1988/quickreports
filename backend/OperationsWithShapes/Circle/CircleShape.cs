﻿using OperationsWithShapes.BaseLogic;

namespace OperationsWithShapes.Circle;

public class CircleShape : Shape
{
    public float Radius { get; private set; }

    public CircleShape(CircleShapeParameter parameter)
        : base(parameter)
    {
        Radius = parameter.Radius;
    }

    public void SetRadios(float radios)
    {
        this.Radius = radios;
    }
}
