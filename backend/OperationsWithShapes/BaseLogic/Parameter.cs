﻿namespace OperationsWithShapes.BaseLogic;

public record Parameter<T>
    where T : class
{
    public T Value { get; set; }
}
