﻿using System.Drawing;

namespace OperationsWithShapes.BaseLogic;

public interface IFillable
{
    Color FillColor { get; set; }
}
