﻿using System.Drawing;

namespace OperationsWithShapes.BaseLogic;
public interface IBorderable
{
    Color BorderColor { get; set; }
    float BorderWidth { get; set; }
}
