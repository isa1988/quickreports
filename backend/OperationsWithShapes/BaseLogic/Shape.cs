﻿using System.Drawing;

namespace OperationsWithShapes.BaseLogic;

public abstract class Shape : IFillable, IBorderable, IPositionable, IShape
{
    public Color FillColor { get; set; }
    public Color BorderColor { get; set; }
    public float BorderWidth { get; set; }
    public PointF Position { get; set; }

    protected Shape()
    {

    }

    protected Shape(ShapeParameter parameter)
    {
        this.FillColor = parameter.FillColor;
        this.BorderColor = parameter.BorderColor;
        this.BorderWidth = parameter.BorderWidth;
        this.Position = parameter.Position;
    }
}
