﻿using System.Drawing;

namespace OperationsWithShapes.BaseLogic;

public interface IPositionable
{
    PointF Position { get; set; }
}
