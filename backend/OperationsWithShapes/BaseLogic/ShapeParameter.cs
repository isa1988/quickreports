﻿using System.Drawing;

namespace OperationsWithShapes.BaseLogic;

public record ShapeParameter
{
    public Color FillColor { get; set; }
    public Color BorderColor { get; set; }
    public float BorderWidth { get; set; }
    public PointF Position { get; set; }
}
