﻿using OperationsWithShapes.BaseLogic;
using System.Drawing;

namespace OperationsWithShapes.Triangle;

public class TriangleEquilateralShape : Shape
{
    public float SideLength { get; private set; }
    public PointF[] Points { get; private set; } = new PointF[3];

    public TriangleEquilateralShape(TriangleEquilateralShapeParameter parameter)
        : base(parameter)
    {
        SideLength = parameter.SideLength;
    }

    public void SetSideLength(float sideLength)
    {
        this.SideLength = sideLength;
    }

    public void SetPoints(PointF[] points)
    {
        this.Points = points;
    }
}
