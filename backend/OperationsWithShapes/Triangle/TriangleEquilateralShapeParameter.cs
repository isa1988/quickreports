﻿using OperationsWithShapes.BaseLogic;

namespace OperationsWithShapes.Triangle;

public record TriangleEquilateralShapeParameter : ShapeParameter
{
    public float SideLength { get; set; }
}
