﻿using OperationsWithShapes.BaseLogic;
using OperationsWithShapes.Square;
namespace OperationsWithShapes;

public class RectangleShape : Shape
{
    public float Width { get; private set; }
    public float Height { get; private set; }

    public RectangleShape(RectangleShapeParameter parameter)
        : base(parameter)
    {
        Width = parameter.Width;
        Height = parameter.Height;
    }

    public void SetWidth(float width)
    {
        this.Width = width; 
    }
    public void SetHeight(float height) 
    {
        this.Height = height; 
    }

}