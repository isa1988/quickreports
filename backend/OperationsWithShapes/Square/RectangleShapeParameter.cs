﻿
using OperationsWithShapes.BaseLogic;

namespace OperationsWithShapes.Square;

public record RectangleShapeParameter : ShapeParameter
{
    public float Width { get; set; }
    public float Height { get; set; }
}
